/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detmatrizes;


/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Laplace {

    private static double determinant;
    public static TimeExec time = new TimeExec();
    
    public static void start(double[][] m) {
        time.start();
        determinant = recursiveLaplace(m);
        time.stop();
    }

    public static double getDeterminant() {
        return determinant;
    }
    
    public static long getElapsedTime() {
        return time.getElapsedTime();
    }
    
    private static double[][] menorComplementar(double[][] m, final int j) {
        int l = m.length;
        int n = l-1;
        double[][] M = new double[n][n];
        int k = 0;
        for (int t = 0; t < n; t++) {
            if (k == j) {
                k++;
            }
            for (int i = 0; i < n; i++) {
                M[i][t] = m[i+1][k];
            }
            k = k+1;
        }
        return M;
    }
   
    private static double recursiveLaplace(double[][] m) {
        if(m.length == 1) {
            return m[0][0];
        } else {
            double det = 0;
            for (int j = 0; j < m.length; j++) {
                det += Math.pow(-1, j+2) * m[0][j] * recursiveLaplace(menorComplementar(m, j));
            }
            return det;
        }
    }
}