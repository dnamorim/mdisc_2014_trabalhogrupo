/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package detmatrizes;

import java.util.Scanner;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Test {

    static Scanner in = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.println("Comparação de Algoritmos para o Cálculo do determinante de uma Matriz");
        System.out.println("LEI-ISEP - MDISC 2014\n");
        
        int order;
        do {
            do {
                System.out.printf("Ordem da Matriz a Gerar (1 a 10, 0 para terminar): ");
                order = in.nextInt();
                in.nextLine();
            } while(order<0 || order > 10);
            
            if(order > 0) {
                System.out.println("\nMatriz Gerada:");
                double[][] m = GenerateMatrix.generate(order);
                GenerateMatrix.showMatrix(m);

                continuar();

                System.out.println("Método de Eliminação de Gauss");
                Gauss.start(m);
                System.out.printf("Valor do Determinante: %.2f%nTempo de Execução: %d ns = %f ms%n", Gauss.getDeterminant(), Gauss.getElapsedTime(), Gauss.time.getElapsedTimeMilli());
                continuar();

                System.out.println("Teorema de Laplace");
                Laplace.start(m);
                System.out.printf("Valor do Determinante: %.2f%nTempo de Execução: %d ns = %f ms%n", Laplace.getDeterminant(), Laplace.getElapsedTime(), Laplace.time.getElapsedTimeMilli());
                continuar();
            }
        } while(order !=0);   
    }
    
    private static void continuar() {
        System.out.println("\nPressione qualquer tecla para continuar.");
        in.nextLine();
    }
}
