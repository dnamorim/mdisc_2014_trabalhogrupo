/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detmatrizes;

import java.util.Formatter;

public class GenerateMatrix {

    static Formatter out = new Formatter(System.out);
    static final int MIN_N = 1, MAX_N = 10;
    
    private static int numAleatorio(int min, int max) {
        int range = (max - min) + 1;     
        return (int)(Math.random() * range) + min;    
    }

    public static void showMatrix(double[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.printf("%6.1f ", mat[i][j]);
            }
            System.out.printf("%n");
        }
    }

    public static double[][] generate(int n) {
        double mat[][] = new double[n][n];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                mat[i][j] = GenerateMatrix.numAleatorio(0, 10);
            }
        }
        return mat;
    }
}
