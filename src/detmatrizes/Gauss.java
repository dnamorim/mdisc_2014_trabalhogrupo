/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package detmatrizes;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 * @author Tiago Ferreira <1130672@isep.ipp.pt>
 * @author André Azevedo <1130740@isep.ipp.pt>
 */

public class Gauss {

    private static double determinant;
    public static TimeExec time = new TimeExec();
    
    public static void start(double[][] m) {
        double[][] mat = copyMatrix(m);
        time.start();
        determinant = detGauss(mat);
        time.stop();
    }
    
    public static double getDeterminant() {
        return determinant;
    }
    
    public static long getElapsedTime() {
        return time.getElapsedTime();
    }
    
    private static double detGauss(double[][] m) {
        double det = 1;
        int n = m.length; //Ordem da Matriz
        int k;
        double coef;
        for (int i=0; i < n-1; i++) {
            if (m[i][i] == 0) {
                k = -1;
                for (int j=i+1; j < n; j++) {
                    if (m[j][i] != 0) {
                        k = j;
                        break;
                    }
                }
                if (k == -1) {
                    det=0;
                    return det;
                }
                for(int j=i; j < n; j++){
                    double temp = m[i][j];
                    m [i][j] = m[k][j];
                    m[k][j] = temp;
                }
                det= (-1)*det;
            }
            for(int j=i+1; j < n; j++){
              coef = -(m[j][i])/(m[i][i]);
              for(k=i ; k < n; k++){
                  m[j][k] = (m[j][k] + coef * m[i][k]);
              }
            }
        }
   
        for(int i=0; i < n; i++){
            det = det*m[i][i];
        }
        return det;
    }

    private static double[][] copyMatrix(double[][] m) {
        double[][] mat = new double[m.length][m[0].length];
        
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                mat[i][j] = m[i][j];
            }
        }
        
        return mat;
    }
}

