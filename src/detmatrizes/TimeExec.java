package detmatrizes;

/*
    Copyright (c) 2005, Corey Goldberg

    StopWatch.java is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
*/

public class TimeExec {
    
    private long startTime = 0;
    private long stopTime = 0;
    private boolean running = false;

    
    public void start() {
        this.startTime = System.nanoTime();
        this.running = true;
    }

    
    public void stop() {
        this.stopTime = System.nanoTime();
        this.running = false;
    }

    
    //elaspsed time in milliseconds
    public long getElapsedTime() {
        long elapsed;
        if (running) {
             elapsed = (System.nanoTime() - startTime);
        }
        else {
            elapsed = (stopTime - startTime);
        }
        return elapsed;
    }

    public double getElapsedTimeMilli() {
        double elapsed;
        if(running) {
            elapsed = ((System.nanoTime() - startTime)/ 1*Math.pow(10, -6)); 
        } else {
            elapsed = ((stopTime - startTime)/ 1*Math.pow(10, -6));
        }
        return elapsed;
    }
}