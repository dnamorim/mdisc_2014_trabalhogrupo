# mdisc_2014_trabalhogrupo

Repositório do Trabalho de Grupo da Unidade Curricular "Matemática Discreta" (MDISC) do 1º Ano da [Licenciatura de Engenharia Informática](http://www.isep.ipp.pt/Course/Course/26) do [Instituto Superior de Engenharia do Porto](http://www.isep.ipp.pt) (LEI-ISEP).

© [Duarte Amorim](https://bitbucket.org/1130674), 2014